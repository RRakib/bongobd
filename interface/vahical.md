# Interface is not supported directly into javascript

As for TypeScript interface acts like a contact for Class or Functions.
Class or Function that uses interface must follow the structure provided in the interface.

If i convert the example into TypeScript Interface it looks like this:

    interface Vehicle {
        set_num_of_wheels: () => number
        set_num_of_passengers: () => number
        has_gas: () => boolean
    }

If a Class implements this interface then the class must have 3 functions:

    - set_num_of_wheels() => It will return an integer / number value
    - set_num_of_passengers() => It will return an integer / number value
    - has_gas() => It will return an boolean value

Car class using this interface:

    class Car implements Vehicle { 
        set_num_of_wheels():number { 
            return 4;
        }

        set_num_of_passengers():number { 
            return 4;
        }

        has_gas():boolean { 
            return true;
        }
    }

    let emp = new Car();

Plane class using this interface:

    class Plane implements Vehicle { 
        set_num_of_wheels():number { 
            return 4;
        }

        set_num_of_passengers():number { 
            return 4;
        }

        has_gas():boolean { 
            return true;
        }
    }

    let emp = new Plane();
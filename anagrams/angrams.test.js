const checkAnagram = require('./anagrams');

let stringSets = [['bleat', 'table', "This is an anagram"], ['bleat', 'tabee', "This is not an anagram"], ['bleat', 'tablee', "This is not an anagram"]];

test.each(stringSets)("Check if two given strings are anagrams", (str1, str2, output) => {
    expect(checkAnagram(str1, str2)).toBe(output);
});
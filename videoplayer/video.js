let vid = document.querySelector('.video');
let play = document.querySelector('.play');
let pause = document.querySelector('.pause');
let playbar = document.querySelector('.playbar');
let fforward = document.querySelector('.fforward');


const togglePlayPause = () => {
    if(vid.paused){
        play.style.display = 'none';
        pause.style.display = 'block';
        vid.play()
    } else {
        play.style.display = 'block';
        pause.style.display = 'none';
        vid.pause()
    }
}

const fastForward = () => {
    vid.currentTime += 3;
}

const rewind = () => {
    vid.currentTime = 0;
}

vid.addEventListener('timeupdate', () => {
    let plbar = vid.currentTime / vid.duration;
    playbar.style.width = plbar * 100 + '%'

    if(vid.paused){
        play.style.display = 'block';
        pause.style.display = 'none';
    }
})